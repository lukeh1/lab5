#include <stdint.h>

// Note struct
typedef struct {
	
	uint32_t loudness;	// amplitude PP of wave
	uint32_t period;		// period of wave
	uint32_t pitch;			// frequence = 1/period
	uint32_t envelope;  // IDK HOW TO DO THIS, BUT I GUESS ITS JUST A SCALING FUNCTION/FACTOR
	
}	Note;


// Song struct
typedef struct {
	
	uint32_t length;    // number of notes in song
	uint32_t notes[100000]; // holds notes in song (dont know how to declare it without spefifying a length
	uint32_t tempo;			// speed of the sound
	uint32_t beat;			// quarter note time
	
} Song;



/*************** Play2_Waves **********************
 * This function creates a chord of two notes 
 * and places the values in the array passed into the function
 *
 * Inputs: *wave[] - array to store new chord in 
 *         freq1   - frequency of first note
 *				 freq2   - frequency of second note
 * Outputs: NONE
 ************************************************/
void Play2_Waves(uint32_t *wave[], uint32_t freq1, uint32_t freq2);


/*************** Play_Song **************************
 * Plays a specific song once the PLAY button is pushed 
 * 
 * Inputs: songPtr  - pointer to Song to be played
 * 				startTime - starting point to play song from 
 *									- set to 0 to play from beginning
 * Outputs: NONE 
 ***************************************************/
void Play_Song(Song *song, uint32_t startTime);


/*************** Pause_Song ****************************
 * Pauses the currently playing song
 * Stores the position in the song's note array so that 
 * the song can be coninued if Play_Song is later called
 *
 * Input: song - current song that is playing
 * Output: NONE
********************************************************/
void Pause_Song(Song *song);


/*************** Rewind_Song ***************************
 * Rewinds the song back to the beginning
 * Resets curSongTime back to 0
 *
 * Inputs: song - ptr to song that is currently playing
 * Outputs: NONE 
 ******************************************************/
void Rewind_Song(Song *song);