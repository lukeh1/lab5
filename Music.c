

#include <stdint.h>
#include <stdlib.h>
#include "tm4c123gh6pm.h"
#include "DAC.h"

// Period =  50000000/64/Freq=781250/Freq
#define C1   373   // 2093 Hz
#define B1   395   // 1975.5 Hz
#define BF1  419   // 1864.7 Hz
#define A1   444   // 1760 Hz
#define AF1  470   // 1661.2 Hz
#define G1   498   // 1568 Hz
#define GF1  528   // 1480 Hz
#define F1   559   // 1396.9 Hz
#define E1   593   // 1318.5 Hz
#define EF1  628   // 1244.5 Hz
#define D1   665   // 1174.7 Hz
#define DF1  705   // 1108.7 Hz
#define C    747   // 1046.5 Hz
#define B    791   // 987.8 Hz
#define BF   838   // 932.3 Hz
#define A    888   // 880 Hz
#define AF   941   // 830.6 Hz
#define G    997   // 784 Hz
#define GF  1056   // 740 Hz
#define F   1119   // 698.5 Hz
#define E   1185   // 659.3 Hz
#define EF  1256   // 622.3 Hz
#define D   1330   // 587.3 Hz
#define DF  1409   // 554.4 Hz
#define C0  1493   // 523.3 Hz
#define B0  1582   // 493.9 Hz
#define BF0 1676   // 466.2 Hz
#define A0  1776   // 440 Hz
#define AF0 1881   // 415.3 Hz
#define G0  1993   // 392 Hz
#define GF0 2112   // 370 Hz
#define F0  2237   // 349.2 Hz
#define E0  2370   // 329.6 Hz
#define EF0 2511   // 311.1 Hz
#define D0  2660   // 293.7 Hz
#define DF0 2819   // 277.2 Hz
#define C7  2986   // 261.6 Hz
#define B7  3164   // 246.9 Hz
#define BF7 3352   // 233.1 Hz
#define A7  3551   // 220 Hz
#define AF7 3762   // 207.7 Hz
#define G7  3986   // 196 Hz
#define GF7 4223   // 185 Hz
#define F7  4474   // 174.6 Hz
#define E7  4740   // 164.8 Hz
#define EF7 5022   // 155.6 Hz
#define D7  5321   // 146.8 Hz
#define DF7 5637   // 138.6 Hz
#define C6  5972   // 130.8 Hz

long StartCritical(void);
void EndCritical(long);

uint32_t Timer0A_Count, Timer2A_Count = 0;

// 8-bit 64-element sine wave
const unsigned short wave[64] = {
  128,140,153,165,177,188,199,209,218,226,234,
	240,245,250,253,254,255,254,253,250,245,240,
  234,226,218,209,199,188,177,165,153,140,
  128,116,103,91,79,68,57,47,38,30,
  22,16,11,6,3,2,1,2,3,6,11,
  16,22,30,38,47,57,68,79,91,103,116
};

typedef struct {
	
	uint32_t loudness;	// amplitude PP of wave
	uint32_t period;		// period of wave
	uint32_t pitch;			// frequence = 1/period
	uint32_t envelope;  // IDK HOW TO DO THIS, BUT I GUESS ITS JUST A SCALING FUNCTION/FACTOR
	
}	Note;

typedef struct {
	
	uint32_t length;    // number of notes in song
	Note *notes; 				// holds notes in song (dont know how to declare it without spefifying a length
	uint32_t tempo;			// speed of the sound
	uint32_t beat;			// quarter note time
	uint32_t curTime;		// current time (position) in the song
	
} Song;

	
/*************** Play2_Waves **********************
 * This function creates a chord of two notes 
 * and places the values in the array passed into the function
 *
 * Inputs: *wave[] - array to store new chord in 
 *         freq1   - frequency of first note
 *				 freq2   - frequency of second note
 * Outputs: NONE
 ************************************************/
void Play2_Waves(uint32_t freq1, uint32_t freq2) {
	
	/*
	 1.  Need to initialize two timers to interrupt at the disired frequencies
	      - Use Timer0A_Init(period) to interrupt at first frequency
				- Use Timer2A_Init(period) to interrupt at second frequency
	 2.  Add the two waves together
	*/
}


/*************** Play_Song **************************
 * Plays a specific song once the PLAY button is pushed 
 * 
 * Inputs: songPtr  - pointer to Song to be played
 * 				startTime - starting point to play song from 
 *									- set to 0 to play from beginning
 * Outputs: NONE 
 ***************************************************/
void Play_Song(Song *song, uint32_t startTime) {
	
	/*
	for(each note in song) {  // length of each note will be defined based on song's beat/tempo
	  get note frequence
	  set timer to interrupt at that frequence
	  play note for note's duration
  }
	*/
	
}

/*************** Pause_Song ****************************
 * Pauses the currently playing song
 * Stores the position in the song's note array so that 
 * the song can be coninued if Play_Song is later called
 *
 * Input: song - current song that is playing
 * Output: NONE
********************************************************/
void Pause_Song(Song *song) {
	// Call methond getSongTime to update global variable CurSongTime (I GUESS I COULD MAKE IT PART OF SONG STRUCT)
	// Turn off interrupts??
}

/*************** Rewind_Song ***************************
 * Rewinds the song back to the beginning
 * Resets curSongTime back to 0
 *
 * Inputs: song - ptr to song that is currently playing
 * Outputs: NONE 
 ******************************************************/
void Rewind_Song(Song *song) {
	// Disable Interrupts to stop playing????
	// reset curSongTime back to 0
}	


// ***************** Timer0A_Init ****************
// Activate TIMER0 interrupts to run user task periodically
// Inputs:  task is a pointer to a user function
//          period in units (1/clockfreq), 32 bits
// Outputs: none
void Timer0A_Init(uint32_t period){long sr;
  sr = StartCritical(); 
  SYSCTL_RCGCTIMER_R |= 0x01;   // 0) activate TIMER0
  //PeriodicTask = task;          // user function
  TIMER0_CTL_R = 0x00000000;    // 1) disable TIMER0A during setup
  TIMER0_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER0_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER0_TAILR_R = period-1;    // 4) reload value
  TIMER0_TAPR_R = 0;            // 5) bus clock resolution
  TIMER0_ICR_R = 0x00000001;    // 6) clear TIMER0A timeout flag
  TIMER0_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI4_R = (NVIC_PRI4_R&0x00FFFFFF)|0x80000000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 35, interrupt number 19
  NVIC_EN0_R = 1<<19;           // 9) enable IRQ 19 in NVIC
  TIMER0_CTL_R = 0x00000001;    // 10) enable TIMER0A
  EndCritical(sr);
}

void TestSong(void) {

	Song oneNote;
	oneNote.length = 2;
	oneNote.notes = (Note*) malloc(sizeof(Note) * oneNote.length);
	
	Note *cur = (Note*) malloc(sizeof(Note));
	cur->loudness = 1;
	cur->period = 2840;
	cur->pitch = 440;
	
	oneNote.notes[0] = *cur;
	
	Note *n2 = (Note*) malloc(sizeof(Note));
	n2->loudness = 1;
	n2->period = 2388;
	
	Timer0A_Init(oneNote.notes[0].period);
	
	/*
	free(cur);
	free(oneNote.notes); */
	
}


// ***************** Timer1A_Init ****************
// Activate TIMER1 interrupts to run user task periodically
// Inputs:  task is a pointer to a user function
//          period in units (1/clockfreq), 32 bits
// Outputs: none
void Timer2A_Init(uint32_t period){long sr;
  sr = StartCritical(); 
  SYSCTL_RCGCTIMER_R |= 0x04;   // 0) activate TIMER0
  //PeriodicTask = task;          // user function
  TIMER0_CTL_R = 0x00000000;    // 1) disable TIMER0A during setup
  TIMER0_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER0_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER0_TAILR_R = period-1;    // 4) reload value
  TIMER0_TAPR_R = 0;            // 5) bus clock resolution
  TIMER0_ICR_R = 0x00000001;    // 6) clear TIMER0A timeout flag
  TIMER0_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI4_R = (NVIC_PRI4_R&0x00FFFFFF)|0x80000000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 35, interrupt number 19
  NVIC_EN0_R = 1<<23;           // 9) enable IRQ 19 in NVIC
  TIMER0_CTL_R = 0x00000001;    // 10) enable TIMER0A
  EndCritical(sr);
}

uint16_t Time;
// Used when only one frequency needs to be outputted
void Timer0A_Handler(void){
  TIMER0_ICR_R = 0x00000004;// acknowledge timer0A timeout
	
	// call DAC_Out for the current position in the sine wave
	Time = (Time + 1)&0x3F;
	DAC_Out(wave[Time]);
	
  //(*PeriodicTask)();                // execute user task
}
	
// Used when a second frequency needs to be outputted
void Timer2A_Handler(void){
  TIMER0_ICR_R = 0x00000001;// acknowledge timer2A timeout
	
	// call DAC_Out for the current position in the sine wave
	
}
