
#include "switch.h"

volatile int32_t SW1;
volatile int32_t SW2;
volatile int32_t SW3;
volatile int32_t Button_Pushed;

/* 
	For example, you could implement two public functions: initialize 
	GPIO (we're using port F, built in switches PF0 and PF4
 	and return the current position of the two switches.
*/
	
	

/********** Switch_Init *********************
 * Initializes the the switches on PF0 and PF4. 
 * Inputs: NONE
 * Outputs: 1 for success
						0 for failures
						Return used for debugging
 *****************************************/
void Switch_Init(void){

	//DisableInterrpts();
	SYSCTL_RCGCGPIO_R |= 0x00000010; // activate port E
  //SW1 = 0;                    // clear semaphores
  //SW2 = 0;
  GPIO_PORTE_AMSEL_R &= ~0x1F;// disable analog function on PE5-4
  GPIO_PORTE_PCTL_R &= ~0x000FFFFF; // configure PE5-4 as GPIO 
  GPIO_PORTE_DIR_R &= ~0x1F;  // make PE5-4 in 
  GPIO_PORTE_AFSEL_R &= ~0x1F;// disable alt funct on PE5-4 
  GPIO_PORTE_DEN_R |= 0x1F;   // enable digital I/O on PE5-4
  GPIO_PORTE_IS_R &= ~0x1F;   // PE5-4 is edge-sensitive 
  GPIO_PORTE_IBE_R &= ~0x1F;  // PE5-4 is not both edges 
  GPIO_PORTE_IEV_R |= 0x1F;   // PE5-4 rising edge event
  GPIO_PORTE_ICR_R = 0x1F;    // clear flag5-4
  GPIO_PORTE_IM_R |= 0x1F;    // enable interrupt on PE5-4
                              // GPIO PortE=priority 2
  NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFFFF00)|0x00000040; // bits 5-7
  NVIC_EN0_R = NVIC_EN0_INT4; // enable interrupt 4 in NVIC
	//GPIO_PORTE_PUR_R |= 0x1F;
}
	
	


/* Return whether SW1 (PF0) is clicked, which means that
	 the motor must be slowed down.
*/
int32_t Switch1_Position (void)
{
	return SW1;  // slow down switch
}

/* Return whether SW1 (PF4) is clicked, which means that
	 the motor must be sped up.
*/
int32_t Switch2_Position (void)
{
	return SW2;	// speed up switch
}

/* This function is called when either switch (PF0/4) is clicked.
*/
void GPIOPortE_Handler(void){
  //GPIO_PORTE_ICR_R = 0x3F;      // acknowledge flag
	
	//reg = GPIO_PORTF_RIS_R;
	/*
	if(PF0 != 1) { Desired_Speed += 5; SW1 = 1; Button_Pushed = 1; GPIO_PORTE_ICR_R = 0x01;}
	else if(PF4 != 1) { Desired_Speed -= 5; SW2 = 1; Button_Pushed = 1; GPIO_PORTE_ICR_R = 0x10;}
	*/
	
	int reg = GPIO_PORTE_RIS_R;

	
	if(reg == 2) 		   { SW1 = 1; GPIO_PORTE_ICR_R = 0x02;}
	else if(reg == 4)  { SW2 = 1; GPIO_PORTE_ICR_R = 0x04;}
	else if(reg == 8)  { SW3 = 1; GPIO_PORTE_ICR_R = 0x08;}
}


