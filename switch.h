#include "tm4c123gh6pm.h"

extern volatile int32_t SW1;
extern volatile int32_t SW2;
extern volatile int32_t SW3;
extern volatile int32_t Button_Pushed;


#define GPIO_LOCK_KEY           0x4C4F434B  // Unlocks the GPIO_CR register
#define PF0       (*((volatile uint32_t *)0x40025004))
#define PF4       (*((volatile uint32_t *)0x40025040))
#define SWITCHES  (*((volatile uint32_t *)0x40025044))
//#define SW1       0x10                      // on the left side of the Launchpad board
//#define SW2       0x01                      // on the right side of the Launchpad board
#define RED       0x02
#define BLUE      0x04
#define GREEN     0x08
#define PF21 (*((volatile uint32_t *)0x40025018))

//volatile int32_t reg;

/********** Switch_Init *********************
 * Initializes the the switches on PF0 and PF4.  
 * Inputs: NONE
 * Outputs: 1 for success
						0 for failures
						Return used for debugging
 *****************************************/
void Switch_Init(void);

/* Return whether SW1 (PF0) is clicked, which means that
	 the motor must be slowed down.
*/
int32_t Switch1_Position(void);

/* Return whether SW1 (PF4) is clicked, which means that
	 the motor must be sped up.
*/
int32_t Switch2_Position (void);

/* This function is called when either switch (PF0/4) is clicked.
*/
void GPIOPortF_Handler(void);


	
